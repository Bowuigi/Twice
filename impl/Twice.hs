module Twice where

import Prelude hiding (id, (.))
import Control.Category

newtype TwiceFromCategory cat = TwiceFromCategory cat
newtype TwiceFst t = TwiceFst t
newtype TwiceSnd t = TwiceSnd t

class TwiceCategory t where
  o1 :: t a b -> t b c -> t a c
  o2 :: t a b -> t b c -> t a c
  id1 :: t a a
  id2 :: t a a

instance Category cat => TwiceCategory (TwiceFromCategory cat) where
  o1 (TwiceFromCategory f) (TwiceFromCategory g) = f >>> g
  o2 = o1
  id1 = TwiceFromCategory Control.Category.id
  id2 = id1

instance Twice t => Category (TwiceFst t) where
  (.) = flip o1
  id = id1

instance Twice t => Category (TwiceSnd t) where
  (.) = flip o2
  id = id2
