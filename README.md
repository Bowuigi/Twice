# Twice

Research about a category with different ways to compose morphisms.

## Quick tour

- [Twice.pi](impl/Twice.pi): Implementation for twice-categories and their relations with the usual categories in Π∀, a simple implementation of a dependently typed language by Stephanie Weirich ([link](https://github.com/sweirich/pi-forall)). Note: The "full" language is used here, although it may typecheck with the earlier versions.
- [Twice.hs](impl/Twice.hs): Implementation for twice-categories and their relations as a Haskell typeclass, using newtypes for the relations between twices and categories.
